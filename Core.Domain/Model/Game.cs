using System.Runtime.Versioning;

namespace Bowling.Core.Domain.Model
{
    public class Game
    {
        public string Id { get; set; }
        public int Score { get; set; }
        public List<Frame> Frames { get; set; } = new List<Frame>();
    }
}