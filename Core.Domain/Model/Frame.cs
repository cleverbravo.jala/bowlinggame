﻿namespace Bowling.Core.Domain.Model
{
    public class Frame
    {
        public string Id { get; set; }
        public string GameId { get; set; }
        public int Number { get; set; } = 0;
        string _first = "-";
        public string First
        {
            get { return _first; }
            set { _first = value; Rolls = 1; }
        }
        string _second = "-";
        public string Second
        {
            get { return _second; }
            set { _second = value; Rolls = 0; }
        }
        public int Rolls { get; set; } = 2;
        public Frame()
        {
            GameId = Guid.NewGuid().ToString();
            Number = 1;
        }
        public Frame(string bowlingId, int number)
        {
            Id = string.Empty;
            GameId = bowlingId;
            Number = number;
        }

    }
}
