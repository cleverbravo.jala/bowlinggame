﻿using Bowling.Core.Domain.Model;

namespace Bowling.Core.Domain.Repositories
{
    public interface IFrameCommands
    {
        public Task SaveFrame(Frame frame);
        public Task<int> UpdateFrame(Frame frame);
    }
}
