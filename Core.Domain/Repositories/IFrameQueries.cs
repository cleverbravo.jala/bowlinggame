﻿using Bowling.Core.Domain.Model;
using System.Runtime.CompilerServices;

namespace Bowling.Core.Domain.Repositories
{
    public interface IFrameQueries
    {
        Task<Frame> GetById(string frameId);
        Task<List<Frame>> GetFramesByGameId(string bowlingId);
        Task<int> GetNextNumber(string bowlingId);
        Task<int> GetCurrentNumber(string bowlingId);
        Task<Frame> GetByNumber(string bowlingId, int number);
    }
}
