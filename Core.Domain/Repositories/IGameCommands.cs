﻿using Bowling.Core.Domain.Model;

namespace Bowling.Core.Domain.Repositories
{
    public interface IGameCommands
    {
        public Task SaveGame(Model.Game bowling);
        public Task<int> UpdateGame(Model.Game bowling);

    }
}
