﻿using Bowling.Core.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Core.Domain.Services
{
    public interface IFrameService
    {
        Task<int> Roll(Frame frame);
        int ScoreFrame(string r1, string r2);
        Task<List<Frame>> GetFrames(string gameId);
    }
}
