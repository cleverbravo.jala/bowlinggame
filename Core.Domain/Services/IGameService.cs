﻿using Bowling.Core.Domain.Model;

namespace Bowling.Core.Domain.Services
{
    public interface IGameService
    {
        Task<string> NewGame();
        Task<int> Roll(Frame frame);
        Task<Game> GetGame(string gameId);
        int ScoreGame(string r1, string r2);
        Task<List<Frame>> GetFrames(string gameId);
    }
}
