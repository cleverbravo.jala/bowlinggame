﻿namespace Bowling.Core.Domain.Utils;

public static class DBContextTypes
{
    public const string Postgres = nameof(Postgres);
    public const string Mongo = nameof(Mongo);
}
