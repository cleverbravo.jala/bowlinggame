﻿namespace Bowling.Core.Domain.Utils
{
    public static class EnvironmentVarNames
    {
        public static string DBContextType { get; } = "DB_CONTEXT";
        public static string MongoUri { get; } = "MONGODB_URI";
        public static string PostgresUri { get; } = "POSTGRES_URI";
    }
}
