﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Core.Domain.Utils
{
    public class EnvironmentVarValues 
    {
        public static Dictionary<string, string> Values = new Dictionary<string, string>();
        static EnvironmentVarValues()
        {
            //Values.Add(EnvironmentVarsNames.DBContextType, LoadEnv(EnvironmentVarsNames.DBContextType));
            Values.Add(EnvironmentVarNames.DBContextType, DBContextTypes.Mongo);
            switch (Values[EnvironmentVarNames.DBContextType]) 
            {
                case DBContextTypes.Postgres:
                    Values.Add(EnvironmentVarNames.PostgresUri, LoadEnv(EnvironmentVarNames.PostgresUri));
                    break;
                case DBContextTypes.Mongo:
                    Values.Add(EnvironmentVarNames.MongoUri, LoadEnv(EnvironmentVarNames.MongoUri));
                    break;
            }
        }
        static string LoadEnv(string env)
        {
            var envValue = Environment.GetEnvironmentVariable(env);
            if (envValue == null)
                throw new Exception($"You must set your '{env}' environment variable.");
            return envValue;
        }
    }
}
