using Bowling.Core.Domain.Repositories;
using Bowling.Core.Domain.Services;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Bowling.Infrastructure.DBPostgresContext;
using Bowling.Infrastructure.DBMongoContext;
using Bowling.Core.Application.Services;
using Bowling.Start;
using Bowling.Core.Domain.Utils;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle


switch(EnvironmentVarValues.Values[EnvironmentVarNames.DBContextType])
{
    case DBContextTypes.Postgres:
        builder.Services.AddPostgresConfiguration();
        break;
    case DBContextTypes.Mongo:
        builder.Services.AddMongoConfiguration();
        break;
}

builder.Services.AddSingleton<IGameService, GameService>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseExceptionHandler(GlobalExceptionHandler.Handler);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
