﻿using Microsoft.AspNetCore.Diagnostics;

namespace Bowling.Start
{
    public class GlobalExceptionHandler
    {
        public static void Handler(IApplicationBuilder app)
        {
            RequestDelegate requestDelegate = new(RequestDelegateException);
            app.Run(requestDelegate);
        }
        public static async Task RequestDelegateException(HttpContext context)
        {
            context.Response.StatusCode = 500;
            context.Response.ContentType = "application/json";
            var exceptionHandlerPathFeature = context.Features?.Get<IExceptionHandlerPathFeature>();
            await context.Response.WriteAsJsonAsync(new { Message = exceptionHandlerPathFeature == null ? "Internal Error." : exceptionHandlerPathFeature.Error.Message });
        }
    }
}
