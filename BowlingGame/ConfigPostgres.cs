﻿using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBPostgresContext;

namespace Bowling.Start
{
    static class ConfigPostgres
    {
        public static IServiceCollection AddPostgresConfiguration(this  IServiceCollection Services) 
        {
            Services.AddSingleton<IGameCommands, Bowling.Infrastructure.DBPostgresContext.Repositories.BowlingCommands>();
            Services.AddSingleton<IGameQueries, Bowling.Infrastructure.DBPostgresContext.Repositories.BowlingQueries>();
            Services.AddSingleton<IFrameCommands, Bowling.Infrastructure.DBPostgresContext.Repositories.FrameCommands>();
            Services.AddSingleton<IFrameQueries, Bowling.Infrastructure.DBPostgresContext.Repositories.FrameQueries>();
            Services.AddDbContext<PGBowlingContext>();
            return Services;
        }
    }
}
