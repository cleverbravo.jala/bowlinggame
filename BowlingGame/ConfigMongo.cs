﻿using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBMongoContext;

namespace Bowling.Start
{
    static class ConfigMongo
    {
        public static IServiceCollection AddMongoConfiguration(this  IServiceCollection Services) 
        {
            Services.AddSingleton<IGameCommands, Bowling.Infrastructure.DBMongoContext.Repositories.GameCommands>();
            Services.AddSingleton<IGameQueries, Bowling.Infrastructure.DBMongoContext.Repositories.GameQueries>();
            Services.AddSingleton<IFrameCommands, Bowling.Infrastructure.DBMongoContext.Repositories.FrameCommands>();
            Services.AddSingleton<IFrameQueries, Bowling.Infrastructure.DBMongoContext.Repositories.FrameQueries>();
            Services.AddDbContext<MongoBowlingContext>();
            return Services;
        }
    }
}
