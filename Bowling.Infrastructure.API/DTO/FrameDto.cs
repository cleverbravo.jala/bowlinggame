﻿namespace Bowling.Infrastructure.API.DTO
{
    public class FrameDto
    {
        public string Pins { get; set; }
    }
}
