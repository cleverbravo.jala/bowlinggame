﻿using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace Bowling.Infrastructure.API.Utils
{
    public class RollValidation
    {
        string pins;
        int frameNumber;
        public RollValidation(string pins, int frameNumber)
        {
            this.pins = pins;
            this.frameNumber = frameNumber;
        }
        public bool IsValid { get => Validate(); }
        public string Message { get; set; }
        bool Validate()
        {
            return IsValidPins() && IsValidFrame();
        }
        bool IsValidPins()
        {
            string pattern = "^([1-9X])+$";
            var matchNotation = Regex.IsMatch(pins, pattern);
            if (pins.Length < 0 || pins.Length > 1 || !matchNotation)
            {
                Message = "Not valid notation for the pins.";
                return false;
            }
            return true;
        }
        bool IsValidFrame()
        {
            if (frameNumber < 0 || frameNumber > 10)
            {
                Message = "Not valid frame";
                return false;
            }
            return true;
        }
    }
}
