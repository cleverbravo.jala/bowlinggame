﻿using Bowling.Core.Domain.Services;
using Bowling.Infrastructure.API.DTO;
using Bowling.Infrastructure.API.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.API.Controllers
{
    [ApiController]
    [Route("api/v1/bowling/frames")]
    public class FrameController : ControllerBase
    {
        private readonly ILogger<BowlingGameController> _logger;
        IFrameService _frameService { get; init; }
        public FrameController(ILogger<BowlingGameController> logger, IFrameService frameService)
        {
            _logger = logger;
            _frameService = frameService;
        }
        /// <summary>
        /// A method to record a roll
        /// </summary>
        /// <param name="gameId">The id of the game.</param>
        /// <param name="frame">The frame dto to recieve the data.</param>
        /// <returns>The Http code, if OK then return the remaining turns</returns>
        [HttpPut("{gameId}")]
        public async Task<IActionResult> Roll([FromRoute]string gameId, [FromBody] FrameDto frame)
        {
            try
            {
                var frames = await _frameService.GetFrames(gameId);
                RollValidation validation = new(frame.Pins, frames.Count);
                if (!validation.IsValid)
                    return BadRequest(new { validation.Message });

                var remaining = await _frameService.Roll(new Core.Domain.Model.Frame { GameId = gameId.ToString(), First = frame.Pins });
                return Ok(new { Score = remaining });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
