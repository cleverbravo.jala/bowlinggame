using Bowling.Core.Domain.Services;
using Bowling.Infrastructure.API.DTO;
using Bowling.Infrastructure.API.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Bowling.Infrastructure.API.Controllers;

[ApiController]
[Route("api/v1/bowling/games")]
public class BowlingGameController : ControllerBase
{

    private readonly ILogger<BowlingGameController> _logger;
    IGameService _gameService { get; init; }

    public BowlingGameController(ILogger<BowlingGameController> logger, IGameService bowlingService)
    {
        _logger = logger;
        _gameService = bowlingService;
    }

    /// <summary>
    /// A method to create a new game
    /// </summary>
    /// <returns>The Http code, if OK then returns the GUID of the new game.</returns>
    [HttpPost]
    public async Task<IActionResult> CreateNewGame()
    {
        try
        {
            var newGameId = await _gameService.NewGame();
            return Ok(new { GameId = newGameId });
        }
        catch (Exception ex)
        {
            return StatusCode((int)HttpStatusCode.Created, ex.Message);
        }
    }


    /// <summary>
    /// A method to get the score of a game.
    /// </summary>
    /// <param name="gameId">The id of the game.</param>
    /// <returns>The Http code, if OK then return the current score.</returns>
    [HttpGet("{gameId}")]
    public async Task<IActionResult> GetGame([FromRoute]string gameId)
    {
        try
        {
            var game = await _gameService.GetGame(gameId);
            return Ok(new { Game = game });
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }
    [HttpGet()]
    public IActionResult HandleError()
    {
        throw new Exception("error to test");
    }
}