﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBMongoContext.Map
{
    internal class MapperFactory
    {
        public static Mapper Create()
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BowlingProfile>();
                cfg.AddProfile<FrameProfile>();
            });
            return new Mapper(mapperConfig);
        }
    }
}
