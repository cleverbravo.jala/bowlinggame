﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBMongoContext.Map
{
    public class FrameProfile : Profile
    {
        public FrameProfile()
        {
            CreateMap<Entities.Frame, Core.Domain.Model.Frame>()
                .ForMember(d => d.Number, o => o.MapFrom(src => src.Number))
                .ForMember(d => d.First, o => o.MapFrom(src => src.First))
                .ForMember(d => d.Rolls, o => o.MapFrom(src => src.Rolls))
                .ForMember(d => d.Second, o => o.MapFrom(src => src.Second))
                .ForMember(d => d.Id, o => o.MapFrom(src => src._id))
                .ForMember(d => d.GameId, o => o.MapFrom(src => src.Game_id.ToString()))
                .ReverseMap();
        }
    }
}
