﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBMongoContext.Entities
{
    public class Game
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int Score { get; set; }
        public List<Frame> Frames { get; set; } = new List<Frame>();
    }
}
