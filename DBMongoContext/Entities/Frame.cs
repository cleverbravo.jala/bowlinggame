﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBMongoContext.Entities
{
    public class Frame
    {
        public ObjectId _id { get; set; }
        public ObjectId Game_id { get; set; }
        public Game Game { get; set; }
        public int Number { get; set; } = 0;
        string _first = "-";
        public string First
        {
            get { return _first; }
            set { _first = value; Rolls = 1; }
        }
        string _second = "-";
        public string Second
        {
            get { return _second; }
            set { _second = value; Rolls = 0; }
        }
        public int Rolls { get; set; } = 2;
        public Frame()
        {
            Number = 1;
        }
        public Frame(ObjectId bowlingId, int number)
        {
            _id = ObjectId.GenerateNewId();
            Game_id = bowlingId;
            Number = number;
        }

    }
}
