﻿using AutoMapper;
using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBMongoContext.Map;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;

namespace Bowling.Infrastructure.DBMongoContext.Repositories
{
    public class GameQueries : IGameQueries
    {
        private readonly MongoBowlingContext _context;
        IMapper _mapper;
        public GameQueries()
        {
            _context = MongoFactory.CreateBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task<Core.Domain.Model.Game> GetBownling(string gameId)
        {
            using (var context = MongoFactory.CreateBowlingContext())
            {
                var result = await context.Games.Where(x => x._id == ObjectId.Parse(gameId)).SingleOrDefaultAsync();
                if (result == null) { throw new NullReferenceException("The BowlingId was not found."); }
                return _mapper.Map<Core.Domain.Model.Game>(result);
            }
        }
    }
}
