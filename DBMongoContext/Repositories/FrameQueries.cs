﻿using AutoMapper;
using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBMongoContext.Map;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using Frame = Bowling.Core.Domain.Model.Frame;

namespace Bowling.Infrastructure.DBMongoContext.Repositories
{
    public class FrameQueries : IFrameQueries
    {
        private readonly MongoBowlingContext _context;
        IMapper _mapper;
        public FrameQueries()
        {
            _context = MongoFactory.CreateBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task<List<Frame>> GetFramesByGameId(string gameId)
        {
            using (MongoBowlingContext context = MongoFactory.CreateBowlingContext())
            {
                var result = await context.Frames.Where(x => x.Game_id == ObjectId.Parse(gameId)).ToListAsync();
                if (result == null)
                    throw new NullReferenceException("The BowlingId was not found.");
                return _mapper.Map<Entities.Frame[], Frame[]>(result.ToArray()).ToList();
            }
        }

        public async Task<Frame> GetById(string frameId)
        {
            var result = await _context.Frames.Where(x => x._id == ObjectId.Parse(frameId)).SingleOrDefaultAsync();
            if (result == null)
                throw new NullReferenceException("The FrameId was not found.");
            return _mapper.Map<Frame>(result);
        }

        public async Task<Frame> GetByNumber(string bowlingId, int number)
        {
            var result = await _context.Games.Where(x => x._id == ObjectId.Parse(bowlingId)).SingleOrDefaultAsync();
            if (result == null) { throw new NullReferenceException("The BowlingId with the specified Frame Number was not found."); }
            var frame = result.Frames.FirstOrDefault(x => x.Number == number);
            return _mapper.Map<Frame>(frame);
        }

        public async Task<int> GetNextNumber(string bowlingId)
        {
            var result = await _context.Games.Where(x => x._id == ObjectId.Parse(bowlingId)).SingleOrDefaultAsync();
            if (result == null) { throw new NullReferenceException("The BowlingId with the specified Frame Number was not found."); }
            return result.Frames.Count + 1;
        }

        public async Task<int> GetCurrentNumber(string bowlingId)
        {
            var result = await _context.Games.Where(x => x._id == ObjectId.Parse(bowlingId)).SingleOrDefaultAsync();
            if (result == null) { throw new NullReferenceException("The BowlingId with the specified Frame Number was not found."); }
            return result.Frames.Count;
        }
    }
}
