﻿using Bowling.Core.Domain.Repositories;
using Bowling.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using AutoMapper;
using Bowling.Infrastructure.DBMongoContext.Map;

namespace Bowling.Infrastructure.DBMongoContext.Repositories
{
    public class GameCommands : IGameCommands
    {
        private readonly MongoBowlingContext _context;
        IMapper _mapper;
        public GameCommands()
        {
            _context = MongoFactory.CreateBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task SaveGame(Game game)
        {
            game.Id = ObjectId.GenerateNewId().ToString();
            var bowlingMapped = _mapper.Map<Entities.Game>(game);
            bowlingMapped._id = ObjectId.Parse(game.Id);
            await _context.Games.AddAsync(bowlingMapped);
            await _context.SaveChangesAsync();
            await Reload(game);
            return;
        }

        public async Task<int> UpdateGame(Game game)
        {
            var document = _context.Games.Where(f => f._id == ObjectId.Parse(game.Id)).FirstOrDefault();
            if (document == null)
                throw new Exception("Can not find the game in the database.");
            document.Score = game.Score;
            var result = await _context.SaveChangesAsync();
            await Reload(game);
            return result;
        }
        private async Task Reload(Game game)
        {
            if (game == null)
                throw new Exception("Game is null");
            var gameSaved = await _context.Games.Where(g => g._id == ObjectId.Parse(game.Id)).FirstOrDefaultAsync();
            game = _mapper.Map<Game>(gameSaved);
        }
    }
}
