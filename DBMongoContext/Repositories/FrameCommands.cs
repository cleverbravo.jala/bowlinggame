﻿using AutoMapper;
using Bowling.Core.Domain.Model;
using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBMongoContext.Map;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Bowling.Infrastructure.DBMongoContext.Repositories
{
    public class FrameCommands : IFrameCommands
    {
        private readonly MongoBowlingContext _context;
        IMapper _mapper;
        public FrameCommands()
        {
            _context = MongoFactory.CreateBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task SaveFrame(Frame frame)
        {
            frame.Id = ObjectId.GenerateNewId().ToString();
            var frameMapped = _mapper.Map<Entities.Frame>(frame);
            frameMapped._id = ObjectId.Parse(frame.Id);
            frameMapped.Game_id = ObjectId.Parse(frame.GameId);
            var game = _context.Games.Where(x => x._id == ObjectId.Parse(frame.GameId)).FirstOrDefault();
            if (game == null)
                throw new Exception("Game not found.");
            game.Frames.Add(frameMapped);

            //await _context.Frames.AddAsync(frameMapped);
            await _context.SaveChangesAsync();
            await Reload(frame);
            return;
        }

        public async Task<int> UpdateFrame(Frame frame)
        {
            var document = _context.Frames.Where(f => f._id == ObjectId.Parse(frame.Id)).FirstOrDefault();
            if (document == null)
                throw new Exception("Can not find the frame in the database.");
            document.First = frame.First;
            document.Second = frame.Second;
            var result = await _context.SaveChangesAsync();
            await Reload(frame);
            return result;
        }
        private async Task Reload(Frame frame)
        {
            if (frame == null)
                throw new Exception("Game is null");
            var frameSaved = await _context.Frames.Where(g => g._id == ObjectId.Parse(frame.Id)).FirstOrDefaultAsync();
            frame = _mapper.Map<Frame>(frameSaved);
        }
    }
}
