﻿using Bowling.Core.Domain.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBMongoContext
{
    public class MongoFactory
    {
        public static MongoBowlingContext CreateBowlingContext()
        {
            var connectionString = EnvironmentVarValues.Values[EnvironmentVarNames.MongoUri];
            if (connectionString == null)
                throw new Exception($"You must set your '{EnvironmentVarNames.MongoUri}' environment variable.");
            var client = new MongoClient(connectionString);
            var db = MongoBowlingContext.Create();
            return db;
        }
    }
}
