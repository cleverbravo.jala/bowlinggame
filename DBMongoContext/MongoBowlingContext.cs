﻿using Bowling.Core.Domain.Utils;
using Bowling.Infrastructure.DBMongoContext.Entities;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDB.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBMongoContext
{
    public class MongoBowlingContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Frame> Frames { get; set; }
        static DbContextOptions<MongoBowlingContext> Options { get; set; }
        static MongoBowlingContext()
        {
            var connectionString = EnvironmentVarValues.Values[EnvironmentVarNames.MongoUri];
            if (connectionString == null)
                throw new Exception($"You must set your '{EnvironmentVarNames.MongoUri}' environment variable.");
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("bowling");
            Options = new DbContextOptionsBuilder<MongoBowlingContext>()
                .UseMongoDB(database.Client, database.DatabaseNamespace.DatabaseName)
                .Options;
        }
        public static MongoBowlingContext Create()
        {
            return new(Options);
        }
        public MongoBowlingContext(DbContextOptions options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Game>()
                .ToCollection("games")
                .HasMany<Frame>(x => x.Frames)
                .WithOne(x => x.Game)
                .HasForeignKey(f => f.Game_id);
            modelBuilder.Entity<Frame>()
                .ToCollection("frames");
        }
    }
}
