﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bowling.Core.Domain.Services;
using Bowling.Core.Domain.Repositories;

namespace Bowling.Core.Application.Test.Services
{
    public class TestBowlingService
    {
        IGameService _sut;

        IGameCommands _bowlingCommands;
        IGameQueries _bowlingQueries;
        IFrameCommands _frameCommands;
        IFrameQueries _frameQueries;

        public TestBowlingService()
        {
            var bowlingCommands = new Mock<IGameCommands>();
            bowlingCommands.Setup(b => b.UpdateGame(It.IsAny<Domain.Model.Game>())).Returns(Task.FromResult(1));
            _bowlingCommands = bowlingCommands.Object;
            var frameCommands = new Mock<IFrameCommands>();
            frameCommands.Setup(f => f.UpdateFrame(It.IsAny<Domain.Model.Frame>())).Returns(Task.FromResult(1));
            _frameCommands = frameCommands.Object;
            _sut = new Application.Services.GameService(_bowlingCommands, _bowlingQueries, _frameCommands, _frameQueries);
        }
        [Theory]
        [InlineData("XXXXXXXXX5", "----------", 255)]
        public void Score(string r1, string r2, int expectedResult)
        {
            var result = _sut.ScoreGame(r1, r2);
            Assert.Equal(expectedResult, result);
        }
    }
}
