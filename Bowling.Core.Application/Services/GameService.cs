﻿using Bowling.Core.Domain.Model;
using Bowling.Core.Domain.Repositories;
using Bowling.Core.Domain.Services;
using Bowling.Core.Domain;
using System.Net;
using System.Runtime.Serialization;

namespace Bowling.Core.Application.Services
{
    public class GameService : IGameService
    {
        IGameCommands _gameCommands;
        IGameQueries _gameQueries;
        IFrameQueries _frameQueries;
        public GameService(IGameCommands bowlingCommands, IGameQueries bowlingQueries, IFrameQueries frameQueries)
        {
            _gameQueries = bowlingQueries;
            _gameCommands = bowlingCommands;
            _frameQueries = frameQueries;
        }
        public async Task<Game> GetGame(string gameId)
        {
            var bowling = await _gameQueries.GetBownling(gameId);
            bowling.Frames = await _frameQueries.GetFramesByGameId(gameId);
            return bowling;
        }

        public async Task<string> NewGame()
        {
            var game = new Game() { Id = string.Empty };
            await _gameCommands.SaveGame(game);
            return game.Id;
        }
    }
}
