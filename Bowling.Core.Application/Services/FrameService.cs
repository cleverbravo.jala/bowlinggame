﻿using Bowling.Core.Domain.Model;
using Bowling.Core.Domain.Repositories;
using Bowling.Core.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Core.Application.Services
{
    public class FrameService : IFrameService
    {
        IFrameCommands _frameCommands;
        IFrameQueries _frameQueries;
        IGameCommands _gameCommands;
        IGameQueries _gameQueries;
        public FrameService(IGameCommands bowlingCommands, IGameQueries bowlingQueries, IFrameCommands frameCommands, IFrameQueries frameQueries)
        {
            _gameQueries = bowlingQueries;
            _gameCommands = bowlingCommands;
            _frameCommands = frameCommands;
            _frameQueries = frameQueries;
        }
        public async Task<List<Frame>> GetFrames(string gameId)
        {
            var frames = await _frameQueries.GetFramesByGameId(gameId);
            return frames;
        }
        public async Task<int> Roll(Frame frame)
        {
            var bowling = await _gameQueries.GetBownling(frame.GameId);
            var frames = await _frameQueries.GetFramesByGameId(frame.GameId);
            frames = frames.OrderBy(x => x.Number).ToList();
            if (frames.Count < 10)
            {
                var last = frames.LastOrDefault();
                if (last != null && last.Second == "-" && last.First != "X")
                {
                    last.Second = frame.First;
                    await _frameCommands.UpdateFrame(last);
                }
                else
                {
                    var newFrame = new Frame(frame.GameId, frames.Count + 1);
                    newFrame.First = frame.First;
                    frames.Add(newFrame);
                    await _frameCommands.SaveFrame(newFrame);
                }
            }
            CalculateScore(bowling, frames);
            await _gameCommands.UpdateGame(bowling);
            return bowling.Score;
        }
        void CalculateScore(Game game, List<Frame> frames)
        {
            game.Score = ScoreFrame(string.Join("", frames.Select(x => x.First).ToList()) + new string('-', 10 - frames.Count),
                string.Join("", frames.Select(x => x.Second)) + new string('-', 10 - frames.Count));
        }
        public int ScoreFrame(string rolls1, string rolls2)
        {
            int score = 0;
            int frameIndex = 0;

            for (int frame = 0; frame < 10; frame++)
            {
                if (rolls1[frameIndex] == 'X') // Strike
                {
                    score += 10 + StrikeBonus(rolls1, rolls2, frameIndex);
                    frameIndex++;
                }
                else if (rolls2[frameIndex] == '/') // Spare
                {
                    score += 10 + SpareBonus(rolls1, rolls2, frameIndex);
                    frameIndex++;
                }
                else // Normal frame
                {
                    score += NormalFrameScore(rolls1[frameIndex], rolls2[frameIndex]);
                    frameIndex++;
                }
            }

            return score;
        }

        private int StrikeBonus(string rolls1, string rolls2, int frameIndex)
        {
            if (frameIndex + 1 >= rolls1.Length) return 0;

            int bonus = ConvertToInt(rolls1[frameIndex + 1]);
            if (rolls1[frameIndex + 1] == 'X' && frameIndex + 2 < rolls1.Length) bonus += ConvertToInt(rolls1[frameIndex + 2]);
            else bonus += ConvertToInt(rolls2[frameIndex]);

            return bonus;
        }

        private int SpareBonus(string rolls1, string rolls2, int frameIndex)
        {
            if (frameIndex + 1 >= rolls1.Length) return 0;

            return ConvertToInt(rolls1[frameIndex + 1]);
        }

        private int NormalFrameScore(char roll1, char roll2)
        {
            return ConvertToInt(roll1) + ConvertToInt(roll2);
        }

        private int ConvertToInt(char roll)
        {
            if (roll == 'X' || roll == '/') return 10;
            if (roll == '-') return 0;
            return int.Parse(roll.ToString());
        }
    }
}
