﻿using Bowling.Core.Application.Services;
using Bowling.Core.Domain.Services;
using Bowling.Start;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Moq;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Diagnostics;
using Bowling.Infrastructure.API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Amazon.Runtime.Internal.Util;
using Microsoft.Extensions.Logging;

namespace Bowling.Infrastructure.API.Test.Controllers
{
    public class TestBowlingGameController
    {
        [Fact]
        public async Task RequestDelegateException_InternalServerError()
        {
            var context = new DefaultHttpContext();
            var response = context.Response;

            await GlobalExceptionHandler.RequestDelegateException(context);

            Assert.Equal((int)HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
