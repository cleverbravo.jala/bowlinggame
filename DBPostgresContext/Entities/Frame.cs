﻿namespace Bowling.Infrastructure.DBPostgresContext.Entities
{
    public class Frame
    {
        public Guid Id { get; set; }
        public Guid BownlingId { get; set; }
        public int Number { get; set; } = 0;
        string _first = "-";
        public string First
        {
            get { return _first; }
            set { _first = value; Rolls = 1; }
        }
        string _second = "-";
        public string Second
        {
            get { return _second; }
            set { _second = value; Rolls = 0; }
        }
        public int Rolls { get; set; } = 2;
        public Frame()
        {
            BownlingId = Guid.NewGuid();
            Number = 1;
        }
        public Frame(Guid bowlingId, int number)
        {
            Id = Guid.NewGuid();
            BownlingId = bowlingId;
            Number = number;
        }

    }
}
