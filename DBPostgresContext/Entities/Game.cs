using System.Runtime.Versioning;

namespace Bowling.Infrastructure.DBPostgresContext.Entities
{
    public class Game
    {
        public Guid Id { get; set; }
        public int Score { get; set; }
        public List<Frame> Frames { get; set; } = new List<Frame>();
    }
}