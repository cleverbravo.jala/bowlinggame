﻿using Bowling.Core.Domain.Utils;
using Bowling.Infrastructure.DBPostgresContext.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bowling.Infrastructure.DBPostgresContext
{
    public class PGBowlingContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Frame> Frames { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = EnvironmentVarValues.Values[EnvironmentVarNames.PostgresUri];
            if (connectionString == null)
                throw new Exception($"You must set your '{EnvironmentVarNames.PostgresUri}' environment variable.");
            optionsBuilder.UseNpgsql(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ConfigurationGame());
            modelBuilder.ApplyConfiguration(new ConfigurationFrame());
        }
    }
}
