﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bowling.Infrastructure.DBPostgresContext
{
    public class ConfigurationGame : IEntityTypeConfiguration<Entities.Game>
    {
        public void Configure(EntityTypeBuilder<Entities.Game> builder)
        {
            builder.ToTable("Games");
            builder.HasKey(x => x.Id);
        }
    }
}
