﻿using Bowling.Infrastructure.DBPostgresContext.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bowling.Infrastructure.DBPostgresContext
{
    public class ConfigurationFrame : IEntityTypeConfiguration<Frame>
    {
        public void Configure(EntityTypeBuilder<Frame> builder)
        {
            builder.ToTable("Frames");
            builder.HasKey("Id");
            builder.Property("Number").IsRequired();
        }
    }
}
