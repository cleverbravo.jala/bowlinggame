﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.DBPostgresContext.Map
{
    public class BowlingProfile : Profile
    {
        public BowlingProfile()
        {
            CreateMap<Entities.Game, Core.Domain.Model.Game>()
                .ForMember(d => d.Id, o => o.MapFrom(src => src.Id.ToString()))
                .ForMember(d => d.Frames, o => o.MapFrom(src => src.Frames))
                .ForMember(d => d.Score, o => o.MapFrom(src => src.Score))
                .ReverseMap();

        }
    }
}
