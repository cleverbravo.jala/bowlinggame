﻿using AutoMapper;
using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBPostgresContext.Map;
using Microsoft.EntityFrameworkCore;

namespace Bowling.Infrastructure.DBPostgresContext.Repositories
{
    public class BowlingQueries : IGameQueries
    {
        private readonly PGBowlingContext _context;
        IMapper _mapper;
        public BowlingQueries()
        {
            _context = new PGBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task<Core.Domain.Model.Game> GetBownling(string gameId)
        {
            var gameGuid = Guid.Parse(gameId);
            var result = await _context.Games.Where(x => x.Id == gameGuid).SingleOrDefaultAsync();
            if (result == null) { throw new NullReferenceException("The BowlingId was not found."); }
            return _mapper.Map<Entities.Game, Core.Domain.Model.Game>(result);
        }
    }
}
