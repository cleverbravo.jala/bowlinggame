﻿using Bowling.Core.Domain.Repositories;
using Bowling.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Bowling.Infrastructure.DBPostgresContext.Map;

namespace Bowling.Infrastructure.DBPostgresContext.Repositories
{
    public class BowlingCommands : IGameCommands
    {
        private readonly PGBowlingContext _context;
        IMapper _mapper;
        public BowlingCommands()
        {
            _context = new PGBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task SaveGame(Core.Domain.Model.Game bowling)
        {
            bowling.Id = Guid.NewGuid().ToString();
            var b = _mapper.Map<Core.Domain.Model.Game, Entities.Game>(bowling);
            await _context.Games.AddAsync(b);
            await _context.SaveChangesAsync();
            return;
        }

        public async Task<int> UpdateGame(Core.Domain.Model.Game bowling)
        {
            var result = await _context.Games.Where(f => f.Id == Guid.Parse(bowling.Id))
                .ExecuteUpdateAsync(x => x.SetProperty(z => z.Score, bowling.Score));
            await _context.SaveChangesAsync();
            return result;
        }
    }
}
