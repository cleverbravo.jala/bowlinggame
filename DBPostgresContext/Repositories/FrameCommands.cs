﻿using AutoMapper;
using Bowling.Core.Domain.Model;
using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBPostgresContext.Map;
using Microsoft.EntityFrameworkCore;

namespace Bowling.Infrastructure.DBPostgresContext.Repositories
{
    public class FrameCommands : IFrameCommands
    {
        private readonly PGBowlingContext _context;
        IMapper _mapper;
        public FrameCommands()
        {
            _context = new PGBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task SaveFrame(Frame frame)
        {
            frame.Id = Guid.NewGuid().ToString();
            var frameMapped = _mapper.Map<Entities.Frame>(frame);
            await _context.Frames.AddAsync(frameMapped);
            await _context.SaveChangesAsync();
            return;
        }

        public async Task<int> UpdateFrame(Frame frame)
        {
            var result = await _context.Frames.Where(f => f.Id == Guid.Parse(frame.Id))
                .ExecuteUpdateAsync(x => x.SetProperty(z => z.First, frame.First)
                                          .SetProperty(z => z.Second, frame.Second));
            await _context.SaveChangesAsync();
            return result;
        }
    }
}
