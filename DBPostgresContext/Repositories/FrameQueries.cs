﻿using AutoMapper;
using Bowling.Core.Domain.Repositories;
using Bowling.Infrastructure.DBPostgresContext.Entities;
using Bowling.Infrastructure.DBPostgresContext.Map;
using Microsoft.EntityFrameworkCore;
using Frame = Bowling.Core.Domain.Model.Frame;

namespace Bowling.Infrastructure.DBPostgresContext.Repositories
{
    public class FrameQueries : IFrameQueries
    {
        private readonly PGBowlingContext _context;
        IMapper _mapper;
        public FrameQueries()
        {
            _context = new PGBowlingContext();
            _mapper = MapperFactory.Create();
        }
        public async Task<List<Frame>> GetFramesByGameId(string bowlingId)
        {
            var result = await _context.Frames.Where(x => x.BownlingId == Guid.Parse(bowlingId)).ToListAsync();
            if (result == null) { throw new NullReferenceException("The BowlingId was not found."); }
            return _mapper.Map<List<Entities.Frame>, List<Frame>>(result);
        }

        public async Task<Frame> GetById(string frameId)
        {
            var result = await _context.Frames.Where(x => x.Id == Guid.Parse(frameId)).SingleOrDefaultAsync();
            if (result == null) { throw new NullReferenceException("The FrameId was not found."); }
            return _mapper.Map<Entities.Frame, Frame>(result);
        }

        public async Task<Frame> GetByNumber(string bowlingId, int number)
        {
            var result = await _context.Frames.Where(x => x.BownlingId == Guid.Parse(bowlingId) && x.Number == number).SingleOrDefaultAsync();
            if (result == null) { throw new NullReferenceException("The BowlingId with the specified Frame Number was not found."); }
            return _mapper.Map<Entities.Frame, Frame>(result);
        }

        public async Task<int> GetNextNumber(string bowlingId)
        {
            var result = await _context.Frames.CountAsync(x => x.BownlingId == Guid.Parse(bowlingId));
            return result + 1;
        }

        public async Task<int> GetCurrentNumber(string bowlingId)
        {
            var result = await _context.Frames.CountAsync(x => x.BownlingId == Guid.Parse(bowlingId));
            return result;
        }
    }
}
