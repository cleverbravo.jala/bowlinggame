﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bowling.Infrastructure.DBPostgresContext.Migrations
{
    /// <inheritdoc />
    public partial class initialcreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bownlings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Score = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bownlings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Frames",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BownlingId = table.Column<Guid>(type: "uuid", nullable: false),
                    Number = table.Column<int>(type: "integer", nullable: false),
                    First = table.Column<string>(type: "text", nullable: false),
                    Second = table.Column<string>(type: "text", nullable: false),
                    Rolls = table.Column<int>(type: "integer", nullable: false),
                    BowlingId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Frames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Frames_Bownlings_BowlingId",
                        column: x => x.BowlingId,
                        principalTable: "Bownlings",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Frames_BowlingId",
                table: "Frames",
                column: "BowlingId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Frames");

            migrationBuilder.DropTable(
                name: "Bownlings");
        }
    }
}
